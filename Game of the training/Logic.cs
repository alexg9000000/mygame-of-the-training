﻿using System;

namespace Game_of_the_training
{
    class Logic
    {
        public static void LogicGame(ConsoleKey command, int xPos, int yPos, int[] valueX, int[] valueY, int posPrincess, Field field)
        {
            bool trap;
            do
            {
                switch (command)
                {
                    case ConsoleKey.LeftArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("0");
                        xPos--;
                        break;
                    case ConsoleKey.UpArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("0");
                        yPos--;
                        break;
                    case ConsoleKey.RightArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("0");
                        xPos++;
                        break;
                    case ConsoleKey.DownArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("0");
                        yPos++;
                        break;
                }
                Console.SetCursorPosition(xPos, yPos);
                Console.WriteLine("-");
                trap = Trap.CheckTrap(xPos, yPos, valueX, valueY);
                field.CheckWall(command, ref xPos, ref yPos);
                if (!trap)
                {
                    trap = true;
                    Random random = new Random();
                    int c = random.Next(1, 10);
                    field.Damage -= c;
                    Console.SetCursorPosition(17, 5);
                    Console.WriteLine("Health: {0} ", field.Damage);
                    if (field.Damage <= 0)
                    {
                        field.DefInfo();
                    Label:
                        command = Console.ReadKey().Key;
                        if (command == ConsoleKey.Enter || command == ConsoleKey.Spacebar)
                        {
                            xPos = 3;
                            yPos = 3;
                            CallAttempt(command, valueX, valueY, posPrincess, xPos, yPos, field);
                            command = Console.ReadKey().Key;
                            continue;
                        }
                        else if (command == ConsoleKey.Escape)
                        {
                            break;
                        }
                        else
                        {
                            goto Label;
                        }
                    }
                }
                if (xPos == 12 && yPos == 12)
                {
                    field.WinInfo();
                Label:
                    command = Console.ReadKey().Key;
                    if (command == ConsoleKey.Enter || command == ConsoleKey.Spacebar)
                    {
                        xPos = 3;
                        yPos = 3;
                        CallAttempt(command, valueX, valueY, posPrincess, xPos, yPos, field);
                        command = Console.ReadKey().Key;
                        continue;
                    }
                    else if (command == ConsoleKey.Escape)
                    {
                        break;
                    }
                    else
                    {
                        goto Label;
                    }
                }
                command = Console.ReadKey().Key;
            } while (trap);
        }
        public static void CallAttempt(ConsoleKey command, int[] valueX, int[] valueY, int posPrincess, int xPos, int yPos, Field field)
        {
            Console.Clear();
            field.BuildWall();
            Trap.CreateTrap(valueX, valueY);
            Console.SetCursorPosition(posPrincess, posPrincess);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine('\u0003');
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.SetCursorPosition(xPos, yPos);
            Console.WriteLine('-');
        }
    }
}
