﻿using System;

namespace Game_of_the_training
{
    class Field
    {
        public int Damage { get; set; }
        public void BuildWall()
        {
            for (int i = 2; i < 14; i++)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.SetCursorPosition(i, 2);
                Console.Write("*");
                Console.SetCursorPosition(i, 13);
                Console.Write("*");
            }
            for (int i = 2; i < 13; i++)
            {
                Console.SetCursorPosition(2, i);
                Console.Write("*");
                Console.SetCursorPosition(13, i);
                Console.Write("*");
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            for (int i = 3; i < 13; i++)
            {
                Console.SetCursorPosition(3, i);
                for (int j = 3; j < 13; j++)
                {
                    Console.Write("0");
                }
            }
            Console.SetCursorPosition(17, 5);
            Console.WriteLine("Health: {0}", Damage);
            Rules();
        }

        public void Rules()
        {
            Console.SetCursorPosition(0, 20);
            Console.WriteLine('\u2190' + " - LEFT");
            Console.WriteLine('\u2191' + " - UP");
            Console.WriteLine('\u2192' + " - RIGHT");
            Console.WriteLine('\u2193' + " - DOWN");
            Console.SetCursorPosition(0, 25);
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("RULES:");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("*");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" - borders of the field");
            Console.WriteLine("The player must reach the opposite end of the field,\n" +
                "where the princess is waiting for him.\n" +
                "The player has 10 points of a health.\n" +
                "There are  randomly set 10 traps on the field.\n" +
                "The damage of trap is determined randomly (from 1 to 10).\n" +
                "Traps on the field are not visible, the player walks blindly.");

        }

        public void CheckWall(ConsoleKey command, ref int xPos, ref int yPos)
        {
            if ((xPos == 2 || xPos == 13 || yPos == 2 || yPos == 13))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                switch (command)
                {
                    case ConsoleKey.LeftArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("*");
                        xPos++;
                        break;
                    case ConsoleKey.UpArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("*");
                        yPos++;
                        break;
                    case ConsoleKey.RightArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("*");
                        xPos--;
                        break;
                    case ConsoleKey.DownArrow:
                        Console.SetCursorPosition(xPos, yPos);
                        Console.WriteLine("*");
                        yPos--;
                        break;
                }
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.SetCursorPosition(xPos, yPos);
                Console.WriteLine("-");
            }
        }
        public void WinInfo()
        {
            Console.SetCursorPosition(17, 7);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("CONGRATULATIONS!!!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(0, 15);
            Console.WriteLine("NEW GAME: CLICK 'Enter' or 'Space'");
            Console.WriteLine("CLOSE: 'Escape'");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public void DefInfo()
        {
            Console.SetCursorPosition(17, 5);
            Console.WriteLine("Health: 0 ");
            Damage = 10;
            Console.SetCursorPosition(17, 7);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("GAME OVER");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(0, 15);
            Console.WriteLine("YOU HAVEN'T A HEALTH\nNEW GAME: CLICK 'Enter' or 'Space'\nCLOSE: 'Escape'");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
