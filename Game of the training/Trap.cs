﻿using System;

namespace Game_of_the_training
{
    class Trap
    {
        public static void CreateTrap(int[] valueX, int[] valueY)
        {
            Random random = new Random();
            for (int i = 0; i < valueX.Length; i++)
            {
                valueX[i] = random.Next(3, 12);
            }
            for (int i = 0; i < valueY.Length; i++)
            {
                valueY[i] = random.Next(3, 12);
            }
            for (int i = 0; i < valueX.Length; i++)
            {
                if (!((valueX[i] == 3 && valueY[i] == 3) || (valueX[i] == 12 && valueY[i] == 12)))
                {
                    Console.SetCursorPosition(valueX[i], valueY[i]);
                    Console.WriteLine("0");
                }
            }
        }
        public static bool CheckTrap(int xPos, int yPos, int[] valueX, int[] valueY)
        {
            bool trap = true;
            for (int i = 0; i < valueX.Length; i++)
            {
                if ((xPos == valueX[i] && yPos == valueY[i]))
                {
                    trap = false;
                    break;
                }
            }
            return trap;
        }
    }
}
