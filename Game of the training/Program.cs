﻿using System;

namespace Game_of_the_training
{
    class Program
    {
        static void Main(string[] args)
        {
            Field field = new Field();
            field.Damage = 10;
            field.BuildWall();
            const int posPrincess = 12;
            Console.SetCursorPosition(posPrincess, posPrincess);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine('\u0003');
            Console.ForegroundColor = ConsoleColor.Gray;
            int xPos = 3;
            int yPos = 3;
            Console.SetCursorPosition(xPos, yPos);
            Console.WriteLine('-');
            int[] valueX = new int[10];
            int[] valueY = new int[10];
            Trap.CreateTrap(valueX, valueY);
            ConsoleKey command = Console.ReadKey().Key;
            Logic.LogicGame(command, xPos, yPos, valueX, valueY, posPrincess, field);
        }
    }
}
